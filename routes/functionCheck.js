  var express = require('express'),
      router = express.Router(),
      mongoose = require('mongoose'), //mongo connection
      bodyParser = require('body-parser'), //parses information from POST
      methodOverride = require('method-override'),
      jwt = require('jsonwebtoken'),
      mongoose = require('mongoose'), //mongo connection

      RBAC = require('rbac2'),
      config = require('config'),
      logger = require('../config/logger');


  router.use(bodyParser.urlencoded({
      extended: true
  }))
  router.use(methodOverride(function(req, res) {
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
          // look in urlencoded POST bodies and delete it
          var method = req.body._method
          delete req.body._method
          return method
      }
  }))


  var app = express();
  var dbSecret = '';
  if (config.has('tokenConfig.secret')) {
      dbSecret = config.get('tokenConfig.secret');
  }
  app.set('superSecret', dbSecret);



  var checkPermission =
      function(req, res, next) {
          logger.info('Inside functionCheck :: checkPermission');
          var rolename = req.headers['rolename'];          
          var actionname = req.headers['actionname'];    
          var rulesForRole = req.headers['rules'];
          logger.info('Inside functionCheck :: checkPermission::ROLENAME as :' + rolename + 'ACTIONNAME as ::' + actionname + 'RULESAS::' + rulesForRole);
          var rules = eval('(' + rulesForRole + ')');
          
          var rback = new RBAC(rules);
          
        rback.check(rolename, actionname, function(err, result) {
              // result: true 
             
              if (result) {
                   logger.info('Inside functionCheck :: checkPermission::Inside rbackcheck result as :' +result);
                  next();
              } else {
                 logger.info('Inside functionCheck :: checkPermission::Inside rbackcheck result as false :' +err);
                  res.json({
                    
                      header: {
                          statuscode: 401,
                          statusmessage: "failure"
                      },
                      data: {
                          errormessage: 'Sorry you dont have permission for ' + actionname
                      }
                    
                  });

              }
          });



      };

      router.post('/verifyToken', function(req, res ) {
    	  console.log('********************* Inside verifyToken ::*************************');
    	  logger.info('Inside verifyToken ::');
    	  
    	  console.log('******************getting token 1 as :' +req.body);
    	  console.log('******************getting token 1 as :' +JSON.stringify(req.body.token));
//         
//         var bearerHeader = req.headers['authorization'];
//         console.log('getting bearer header as:' +bearerHeader);
//         
         var token = req.body.token;
//         if (typeof bearerHeader !== 'undefined') {
//             var bearer = bearerHeader.split(" ");
//             
//             token = bearer[1];
//             
//             
//         }   
//         
         console.log('Inside verifyToken ::getting token as ::' +token);
         if(token){
        	   jwt.verify(token, app.get('superSecret'), function(err, decoded) {
             	  
                   if (err) {
                       logger.info('Inside functionCheck :: checkToken::fails during token varification');
                       console.log('TelerayService::Inside functionCheck :: checkToken::fails during token varification');

                       return res.json({                           
                           header: {
                               statuscode: 400,
                               statusmessage: "failure"
                           },
                           data: {
                               errormessage: 'sorry token verification fails' +err
                           }
                         
                       });
                      // return callback(false);
                   } else {
                	   //res.send(decoded);
                	   console.log('token verification done successfully');
                	   return res.json({
                           
                           header: {
                               statuscode: 200,
                               statusmessage: "success"
                           },
                           data: decoded
                         
                       });
                       // if everything is good, save to request for use in other routes
                     // return callback(true);
                   }
               });
         }
        
    	  
      });
  var checkToken = function(token , callback) {
  console.log('Inside teleray-token-service :: checkToken ::');     
      // decode token
      if (token) {
        logger.info('Inside functionCheck :: checkToken::get token');
        console.log('TelerayService::Inside functionCheck :: checkToken::get token');

          // verifies secret and checks exp
          jwt.verify(token, app.get('superSecret'), function(err, decoded) {
        	  
              if (err) {
                  logger.info('Inside functionCheck :: checkToken::fails during token varification');
                  console.log('TelerayService::Inside functionCheck :: checkToken::fails during token varification');

                 // return callback(false);
              } else {
                  // if everything is good, save to request for use in other routes
                // return callback(true);
              }
          });

      } else {

         return callback(false);
      }
  };

//write a callback methos for token creation for valid user
  router.post('/createToken', function(req, res ) {
	  console.log('Inside createToken user ass ::' +JSON.stringify(req.body));
	  logger.info('Inside createToken user ass ::' +JSON.stringify(req.body));	  
	  
	  
     var user = req.body;
      generateToken(user , function(resToken){
    	  console.log('inside generate token' + JSON.stringify(resToken));
    	 // return res.send(resToken);
    	
    	  if(resToken){
	    	  return res.json({
	              header: {
	                  statuscode: 200,
	                  statusmessage: "success"
	              },
	              data: {
	                  token: resToken
	              }
	
	          });
    	  
    	  }else{
    		  
    		  return res.json({
                  header: {
                      statuscode: 400,
                      statusmessage: "failure"
                  },
                  data: {
                      errormessage: 'Problem in retriving token for user'
                  }

              });
    	  }
    	  
      });
  });

  var generateToken = function(user , callback) {
       logger.info('Inside functionCheck :: generateToken :: +' +user);
       console.log('Inside functionCheck :: generateToken :: +' +user);
      
      var dbSecret = '';
      var token = '';
      if (config.has('tokenConfig.secret')) {
          dbSecret = config.get('tokenConfig.secret');
          logger.info('Inside functionCheck::generateToken ::getting secret as :' + dbSecret);
          console.log('Inside functionCheck::generateToken ::getting secret as :' + dbSecret);
      }
      app.set('superSecret', dbSecret);
       token = jwt.sign(user, app.get('superSecret'), {
          expiresIn: 1440 // expires in 24 hours
      });
      console.log('getting token as :' +JSON.stringify(token));
      return callback(token);

  };

  
  module.exports = router;
  module.exports.checkPermission = checkPermission;
  module.exports.checkToken = checkToken;
  module.exports.generateToken = generateToken;  